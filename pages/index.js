import Head from "next/head";
import Layout, { siteTitle } from "../components/layout";
import utilStyles from "../styles/utils.module.css";
import { getSortedPostData } from "../lib/post";
import Link from "next/dist/client/link";
import Date from "../components/date";

export async function getStaticProps() {
  const allPostData = getSortedPostData();
  return {
    props: {
      allPostData,
    },
  };
}

export default function Home({ allPostData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>
          I started using the computer when I was a kid, mostly playing games on
          the desktop in my room, then I became curious about how these things
          work that I began to ransack so many parts of each application on the
          Operating System, then my curiosity became a dream. I had nobody to
          put me through or guide me, all I knew was that I wanted to be a
          software developer and I have to study Computer Science in the
          Tertiary Institution. Before I got into University, immediately after
          Secondary education I went for training as a Computer Engineer, during
          that period also began to love Graphic design which I learned on my
          own and I started working with. When the time came to get into the
          University, picked Computer Science a the course of study but did not
          get the course and I had to study Geography and Planning with the goal
          of switching, as time goes it was seeming impossible, but what gave me
          comfort was that I knew there are many Computer Institute I could go
          to after my years in the University. Immediately after University, I
          started working as a freelance graphic designer and as a computer
          engineer with a small organization (Tunnex Communication) in Lagos,
          Nigeria. Alongside I started learning about programming, the first
          thing I learned was HTML, CSS, and a bit of Javascript. Eventually,
          the opportunity to begin my journey to my dreamland came and I took
          it, I attended Decagon Institute and I decided to learn Java
          Programming language, luckily enough for me, Decagon was an Institute
          that allows you learn to become a full-stack developer which I took
          advantage of and now I am happy I am seeing my dream come true
          becoming a software developer.
        </p>
        <p>
          (This is a sample website - you’ll be building a site like this on{" "}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>

      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {allPostData.map(({ id, date, title }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/posts/${id}`}>
                <a>{title}</a>
              </Link>
              <br />
              <small className={utilStyles.lightText}>
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
}
